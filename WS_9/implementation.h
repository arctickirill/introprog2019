//
// Here you can find answers for tasks from Workshop 8
//

#ifndef WORKSHOPS_IMPLEMENTATION_H
#define WORKSHOPS_IMPLEMENTATION_H

#define _USE_MATH_DEFINES
#include <iostream>
#include <vector>
#include <fstream>
#include <sstream>
#include <set>
#include <map>
#include <cmath>


typedef std::vector<std::vector<std::string>> DataTable;
void fillVectorByDataFromFile(std::istream& inputFile, DataTable& data)
{
    std::string line;

    while (!inputFile.eof() && inputFile.good ())
    {
        std::vector<std::string> row;
        std::getline(inputFile, line);
        std::stringstream lineStream(line);
        std::string field;

        int field_i = 0;
        while (!lineStream.eof() && lineStream.good())
        {
            std::getline(lineStream,field,',');

            if ((field_i > 0 && field_i < 5) || field_i == 8 || field_i == 9)
            {
                row.push_back(field);

            }
            ++field_i;
        }

        data.push_back(row);
    }

}

void howManyCountries(const DataTable& data, std::set<std::string>& countries)
{
    // filling
    for (DataTable::const_iterator it = data.begin(); it != data.end(); ++it)
    {
        if (it == data.begin())
            continue;
        countries.insert((*it)[3]);
    }

    // printing
    for (std::set<std::string>::const_iterator it = countries.begin(); it != countries.end(); ++it)
    {
        std::cout << (*it) << '\n';
    }

}

void findCapital(const DataTable& data, std::map<std::string,std::string>& countriesAndCapitals)
{
    DataTable::const_iterator it = data.begin()+1;
    while (it != data.end())
    {
        if ((*it)[4] == "primary")
            countriesAndCapitals[(*it)[3]] = (*it)[0];
        it++;
    }

    for (std::map<std::string,std::string>::const_iterator it = countriesAndCapitals.begin();
        it != countriesAndCapitals.end(); ++it)
    {
        std::cout << "Country: " << it->first << " <--> Capital: " <<it->second << '\n';
    }


}


#endif //WORKSHOPS_IMPLEMENTATION_H
