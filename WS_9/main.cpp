/*******************************************************************************
 *
 * Workshop 9
 *
 * 1) Download the dataset (https://simplemaps.com/data/world-cities), unzip and use it (.csv) in current workshop.
 *
 * 2) Create a vector<vector<string>> worldCities (vector of lines) where you will store the data.
 * Use worldCities for next tasks.
 *
 * 3) Create a function called howManyCountries. You should count countries and print them out.
 *
 * 4) Create a function which prints countries and its capital.
 *
 * 5) Create a function called uninhabitedLongitude. You should find longitudes (rounding to ceil, 20.7458 -> 20) where
 * there are no cities and print this longitudes out.
 *
 * 6) Find and save to file "population.txt" population by countries and cities. (use map<string,map<string,int>>).
 * If there are no info in population field use "?"-sign. Table should be looks like:
 * Russia: N_1
 * -> Moscow : N_1_1 / N_1
 * -> Saint-Petersburg : N_1_2 / N_1
 * -> ...
 *
 * USA: N_M
 * -> Chicago : N_2_1 / N_2
 * -> NY : N_2_2 / N_2
 * ->
 * ...
 *
 * where N_i -- population of considered country (if not filled use "?"),
 * N_i_i -- population of i-city (if not filled use "?"),
 * N_i_i / N_i  population of the city relative to the country ("?" if some field is not filled)
 *
 * 6.1) Create a function to print some country and its info.
 *
 * 7) Create a structure CityCoordinates for latitude and longitude. Implement methods:
 *
 * 7.1) print (in following format: latitude: N, longitude: M)
 * 7.2) return distance (distance between cities (two paired coordinates -- distance(pair(lat_1,lng_1),pair(lat_2,lng_2))
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * Workshop 10
 *
 * Use dataset from Workshop 9
 *
 * 1) Create structure City {city_ascii, CityCoordinates (Create structure Coordinates for lat and lng),
 * country, population} and fill vector<City> only by capitals of all countries (population should be calculated for
 * whole country)
 *
 * 1.1) Print Coordinates in the following format: latitude: LAT, longitude: LNG)
 * 1.2) Print City in the following format: city: NAME, country: NAME, population: NUM, coors: (lat,lng))
 * 1.3) Implement structure's methods for it
 *
 * 2) Implement method DistanceToCity(const City& city):
 * using Haversine formula for the great-circle distance between two points
 * (https://en.wikipedia.org/wiki/Haversine_formula)
 *
 * for PI(M_PI) use #define _USE_MATH_DEFINES
 * #include <cmath>
 * arcsin <-> asin
 *
 * 3) Operator overloading (+,cout,cin) for CityCoordinates
 * using:
 * * overloading through the usual function
 * * friend CityCoordinates operator+(const CityCoordinates& CC1, const CityCoordinates& CC2);
 * * CityCoordinates operator+(const CityCoordinates& CC1, pair<double,double>) and in reverse order
 * * friend std::ostream& operator<< (std::ostream& out, const CityCoordinates& CC);
 * * friend std::istream& operator>> (std::istream& in, CityCoordinates& CC);
 *
 * P.S. don't forget return return out and in;
 *
 * 4) Operator overloading (<,>,=) for City {population} and cout
 ******************************************************************************/

#include "implementation.h"


int main()
{
    DataTable worldCities;

    const std::string INP_FILE_NAME = "introprog2019/WS_9/worldcities.csv";
    std::ifstream inputFile;
    inputFile.open(INP_FILE_NAME);
    fillVectorByDataFromFile(inputFile,worldCities);
    inputFile.close();

    for (std::vector<std::string> line: worldCities)
    {
        for (std::string field : line)
            std::cout << field << '\t';
        std::cout<<'\n';
    }

    std::set<std::string> countries;
    howManyCountries(worldCities,countries);

    std::cout<< "N countries: " << countries.size()<<'\n';

    std::map<std::string,std::string> countriesAndCapitals;
    findCapital(worldCities,countriesAndCapitals);

    return 0;

}